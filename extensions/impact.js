class ImpactExtension {

    constructor() {
        this.options = {
            position: `top`,
            height: `5px`,
            color: 'cyan',
        };
    }

    init() {
        this.renderAll();
        this.resize();

        big.onNext(slideDiv => {
            setTimeout(() =>
                this.resize([...slideDiv.querySelectorAll('.impact')])
            , 1); // force after slide is visible
        });

        big.onModeChange(mode => {
            if (/(jump|print)/.test(mode)) {
                this.resize();
            }
        });

        return Promise.resolve();
    }

    renderAll() {
        [...document.body.querySelectorAll('[class^="lang-impact"]')]
        .map(element => {
            this.ImpactDiv = document.createElement("div");
            this.ImpactDiv.className = 'impact' + element.className
                .replace(/lang\-impact/, '')
                .replace(',', ' ');
            this.ImpactDiv.innerHTML = element
                .innerText
                .split('\n')
                .filter(x => !!x)
                .map(x => `<span class='line' style="
                    white-space: nowrap;
                    line-height: .95em;
                    ${x.length > 15 ? 'letter-spacing: 0.05em;' : ''}
                ">${x}</span>`)
                .join('\n');

            element.parentNode.style = 'display: none';
            element.parentNode.parentNode.appendChild(this.ImpactDiv);
        });
    }

    resize(elements) {
        (elements || [...document.body.querySelectorAll('.impact')])
        .map(impactDiv => {
            let slideContainer = impactDiv.parentNode.parentNode;
            this.adjust(
                slideContainer.firstChild,
                [...impactDiv.querySelectorAll('.line')],
                slideContainer.clientWidth,
                slideContainer.clientHeight,
            );
        });
    }

    adjust(slideDiv, elements, width, height) {
        elements
        .map(element => {
            element.style.fontSize = '0px';
            return element;
        })
        .map(element => {
            let fontSize = height;
            [100, 50, 10, 2].forEach(function (step) {
                for (; fontSize > 0; fontSize -= step) {
                    element.style.fontSize = fontSize + 'px';
                    if (slideDiv.offsetWidth <= width) {
                        break;
                    }
                }
                fontSize += step;
            });
        })
    }
}

window.impact = new ImpactExtension();