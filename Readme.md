# Upgraded Big Presentation

Extended version of [big-presentation](https://www.npmjs.com/package/big-presentation)

Live at: [big-presentation.surge.sh](http://big-presentation.surge.sh)

## Setup

- Run `yarn`

### Scripts

 Command  | Description
----------|---------
 `build`  | create index.html from index.md
 `offline`| create index.offline.html
 `watch`  | rebuild on index.md change
 `serve`  | start live-server
 `expose` | deploy to [surge.sh](https://surge.sh)
 `dev`    | development mode: `watch` + `serve`

### Usage

1. Clone or fork this project
1. Run `yarn dev`
1. Edit index.md
1. Adjust styles if needed
1. Extend with your own features if you have more time

### Export

- See [big-out](https://gitlab.com/jsek/big-out)

## TODO

- [x] Grid
- [ ] Export script
- [x] Impact (scaled)
- [x] Background image
- [x] Progress bar