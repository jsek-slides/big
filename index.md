# Big Presentation

---

# ![bg:Logo](https://raw.githubusercontent.com/tmcw/big/HEAD/.github/logo.png)

---

- ① Why __*another one*__?
- ② How to `use` it?
- ③ How to *__extend it__*?
- ④ Export to PDF

---

<p class="q"><span class="circle">1</span></p>

---

### Simple

- **`t`**alk mode
- **`j`**ump mode
- **`p`**rint mode  

---

### Powerful

- just a webpage
- include demos, videos, iframes

---

### For developers

- Markdown <small>(HTML)</small>
- extensible

---

### Responsive

<notes>
Adapts to presentation device
</notes>

---

### For talking

<notes>
Not for sharing

- Write little; talk a lot
-- Big is pretty nice for a couple of words, or short sentences
</notes>

---

<p class="q"><span class="circle">2</span></p>

---

[big-presentation](https://www.npmjs.com/package/big-presentation)

---

## What is it made of?

```tree
/
├───/images
│   └───/{...}
│
├───/big.css            # Styles
├───/big.js             # Logic
├───/highlight.css
├───/highlight.js
└───/index.md           # Slides
    └───/index.html     # Generated page
```

---

```shell
> npm install -g big-presentation
```

<hr>

```shell
> big-presentation-init
> big-presentation-compose
> big-presentation-serve
> big-presentation-offline
```

---

### Init <small>/ Clone / Fork</small>

[jsek-slides/big](https://gitlab.com/jsek-slides/big)

---

Notes in DevTools Console

```markdown
> _Fear_ is the path to the **dark side**

<notes>
- Consequences of fear
- Origins of fear
- Unconscious results of fear
- Cost-effective decisions 
</notes>
```

---

> _Fear_ is the path to the **dark side**

<notes>
- Consequences of fear
- Origins of fear
- Unconscious results of fear
- Cost-effective decisions 
</notes>

---

<p class="q"><span class="circle">3</span></p>

---

### What makes it different?

---

```impact
easily code
new features!
```

<notes>
- Compare with PowerPoint, Marp
</notes>

---

```impact
You want ?
You can !!!
```

---

```impact,narrow
Focus on
your ideas
not designing slides
```

---

#### For example...

---

<p class="q"><span class="circle">!</span></p>

---

In slide
```markdown
<p class="q"><span class="circle">!</span></p>
```

<hr>

In big.css
```css
.q {
  align-items: center;
  display: flex;
}
.q .circle {
  width: 1.3em;
  text-align: center;
  border: .04em solid;
  border-radius: 50%;
  line-height: 1.3em;
  margin: auto;
}
```

---

|Extension | Comment
| - | -
| Split view | some additional CSS in big.css, not working on narrow viewport 😐
| Graphs | based on [mermaid](https://mermaidjs.github.io/), easier to change than e.g. Visio graphs, but have some issues on larger resolutions
| Grid | renders CSS grid styles based on simple Markdown, experimental but working suprisingly well 😃
| Progress | simple line on top
| Impact | scaling lines independently gives poster-like view

---

```markdown
<p class="q split"><span class="circle">!</span></p>

### Extensions I've made

- split view
- graphs
- grid
```

---

<p class="q split"><span class="circle">!</span></p>

### Extensions I've made

- split view
- graphs
- grid

---

```markdown
<pre class="mermaid" style="font-size: 30px">
graph LR
    index.md  -- Compose  --> index.html
    big.js    -- Compose  --> index.html
    big.css   -- Compose  --> index.html
    index.html -- Serve   --> http://localhost:8080
</pre>
```

---

<pre class="mermaid" style="font-size: 30px">
graph LR
    index.md  -- Compose  --> index.html
    big.js    -- Compose  --> index.html
    big.css   -- Compose  --> index.html
    index.html -- Serve   --> http://127.0.0.1:8080
</pre>

---

```markdown
<pre class="mermaid" style="font-size: 25px">
sequenceDiagram
    Developer ->> Git       : Merge
    Git       ->> TeamCity  : Checkout
    TeamCity  ->> TeamCity  : Build
    TeamCity  ->> TeamCity  : Test
    TeamCity  ->> TeamCity  : Pack
    TeamCity  ->> Octopus   : Push
    TeamCity  ->> Octopus   : Release
    Octopus   ->> Tentacles : Deploy...
    Octopus   ->> Tentacles : Smoke test
    Octopus   ->> TeamCity  : Result
    Octopus   ->> TeamCity  : Artifacts
    TeamCity  ->> Git       : Tag
</pre>
```

---

<pre class="mermaid" style="font-size: 25px">
sequenceDiagram
    Developer ->> Git       : Merge
    Git       ->> TeamCity  : Checkout
    TeamCity  ->> TeamCity  : Build
    TeamCity  ->> TeamCity  : Test
    TeamCity  ->> TeamCity  : Pack
    TeamCity  ->> Octopus   : Push
    TeamCity  ->> Octopus   : Release
    Octopus   ->> Tentacles : Deploy...
    Octopus   ->> Tentacles : Smoke test
    Octopus   ->> TeamCity  : Result
    Octopus   ->> TeamCity  : Artifacts
    TeamCity  ->> Git       : Tag
</pre>

---

<p class="q"><span class="circle">!</span></p>

<hr>

[Gantt diagrams](https://mermaidjs.github.io/gantt.html) are also supported, but require some CSS to adapt to dark scheme

---

    ```grid(3:2,#1da06e:#183243,gap:.3em)
    grid | is  | super
    fun  | and | easy
    ```

---

```grid(3:2,#1da06e:#183243,gap:.3em)
grid | is  | super
fun  | and | easy
```

---

    ### My Extensions

    ```grid(3:1,#777608:#5E073A,gap:.2em,thin)
    split view | graphs | grid
    ```

---

### My Extensions

```grid(3:1,#777608:#5E073A,gap:.2em,thin)
split view | graphs | grid
```

---

    ```grid(1:2,#151515:invert,bold)
    clean code | simplicity
    ```

---

```grid(1:2,#151515:invert,bold)
clean code | simplicity
```

---

    ```grid(2:1,#bf360c:#065fa3,bold)
    Custom scripts | Octopus Deploy
    ```

---

```grid(2:1,#bf360c:#065fa3,bold)
Custom scripts | Octopus Deploy
```

---

### You can do more

- presentation control panel in separate tab
- automatic links check
- progress bar
- custom directives `<!-- ... -->`
- <span class="blink">blinking</span>

---

```grid(1:1,transparent,funky,thin)
SKY IS THE LIMIT
```


---

<p class="q"><span class="circle">4</span></p>

---

### PDF Export

[jsek/big-out](https://gitlab.com/jsek/big-out)

---

![](images/pdf.png)

---

Based on Puppeteer and PDF Toolkit

```js
(async () => {
    const browser = await puppeteer.launch();
    await Promise.all(options
        .range
        .map(async (i) => {
            let page = await open(browser, `localhost:8080/#${i}`); 
            await print(page, i);
        })
    );
    await browser.close();
    await mergePages();
})()
```

---

```js
async function open (browser, url) {
    const page = await browser.newPage();
    await page.setViewport({width: 1024, height: 640});
    await page.waitForFunction('window.innerWidth > 1000');
    await page.emulateMedia('screen');
    await page.goto(url);
    return page;
}

async function print (page, number) {
    return await page.pdf({
        path: `pages/${number}.pdf`,
        printBackground: true,
        width: '1024 px',
        height: '640 px',
        margin: { top: 0, right: 0, bottom: 0, left: 0 }
    });
}
```

---

### Summary 
 
- For developers
- Freedom
- Fun

---

##### Thanks :-)

---

Different slides, same story:
- [mdznr.github.io/big](http://mdznr.github.io/big)

Advice about using Big:
- [awoodruff.github.io/big-advice](https://awoodruff.github.io/big-advice)